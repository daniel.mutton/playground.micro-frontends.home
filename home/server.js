const express = require('express');
const server = express();

server.use(express.static('public'));

const port = process.env.PORT || 9010;
server.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
