import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

class Map extends HTMLElement {
  attachedCallback() {
    ReactDOM.render(<App />, this.createShadowRoot());
  }
}
document.registerElement('micro-frontends-map', Map);
