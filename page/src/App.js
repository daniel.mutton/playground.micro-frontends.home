import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        I'm a home page!
        <micro-frontends-map />
      </div>
    );
  }
}

export default App;
