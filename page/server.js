const express = require('express');
const server = express();

server.use(express.static('dist'));

const port = process.env.PORT || 8090;
server.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
